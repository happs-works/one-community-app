/**
 * View Models used by Spring MVC REST controllers.
 */
package com.happs.onecommunity.web.rest.vm;
